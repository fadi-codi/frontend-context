
import Styles from './navbar.module.css'
import { Link } from 'react-router-dom';
const NavBar = () => {
return(

    <div className={Styles.navWrapper}>
    <nav className={Styles.navContainer}>
    <Link to="/" className={Styles.logo}>Books.</Link>
        <div className={Styles.linksContainer}>
            <Link to="/">Home</Link>

            <Link to="/login">Login</Link>



            <Link to="/not-found">Not Found</Link>
        </div>
      
        {/* <Link className={Styles.dashboardButton} to="/dashboard">Dashboard</Link> */}
        
    </nav>
    </div> 
)

}

export default NavBar;