import Styles from './loginForm.module.css'
import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Spinner from '../../../src/images/spinner.gif';

const LoginForm = () => {
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    const submitHandler = async (e) => {
        e.preventDefault();
        setLoading(true);
        if (!email || !password) {
            console.log("ENTER EMAIL OR PASSWORD")
            setLoading(false);
            return;
        }


        try {
            const config = {
                headers: {
                    "Content-type": "application/json",
                },
            };

            const { data } = await axios.post(
                "http://localhost:8000/api/auth/login",
                { email, password },
                config
            );


            localStorage.setItem("userInfo", JSON.stringify(data));
            setLoading(false);
            navigate("/");

        } catch (error) {

            console.log(error)

            setLoading(false);
        }
    };


    return (
        <form className={Styles.loginForm} >
            <h1 className={Styles.loginTitle}>Login</h1>

            <div className={Styles.inputWrapper}>
                <input type='text'
                    onChange={(e) => { setEmail(e.target.value) }}
                    className={Styles.formInput} name="email" placeholder='Email' />
            </div>

            <div className={Styles.inputWrapper}>
                <input type='password'
                    onChange={(e) => { setPassword(e.target.value) }}
                    className={Styles.formInput} name="password" placeholder='Password' />
            </div>
{loading ? (<img src={Spinner}/> ):(
            <button onClick={(e) =>submitHandler(e)} className={Styles.loginButton}>Login</button>
)}
        </form>
    )

}

export default LoginForm;