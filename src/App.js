import './App.css';
import { Route, Routes } from "react-router-dom"
import LoginPage from './pages/login/loginPage.js';
import HomePage from './pages/home/home';
import NotFound from './pages/notFound/notFound';

function App() {
  return (
    <div className="App">
      
        <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/login" element={<LoginPage />} />
      <Route path="/not-found" element={<NotFound />} />
    </Routes>

   </div>
  );
}

export default App;
