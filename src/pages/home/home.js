import Styles from './home.module.css'
import Hero from '../../components/hero/hero';
import Toggle from '../../components/themeToggle/toggle';
const HomePage = () => {

    return (
        <div className={Styles.homeContainer} >
            <Toggle />
            <Hero />
        </div>
    )

}

export default HomePage;