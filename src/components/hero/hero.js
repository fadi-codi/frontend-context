import image from '../../images/hero.webp'
import NavBar from '../header/navbar'
import Styles from './hero.module.css'

const Hero = () => {

return(
    <div className={Styles.hero}>
    <div className={Styles.black}></div>
    <NavBar/>
    <img src={image} alt='story' />

    <div className={Styles.heroHeader}>
    Get immersed in your <span>reading</span> journey
    </div>
</div>
)

}

export default Hero;